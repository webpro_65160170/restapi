import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemperatureService } from './temperature.service';

@Controller('temperature')
export class TemperatureController {
  constructor(private readonly temperatuerService: TemperatureService) {}
  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    return this.temperatuerService.convert(parseFloat(celsius));
  }

  @Post('convert')
  convertByPost(@Body('celsius') celsius: number) {
    return this.temperatuerService.convert(celsius);
  }

  @Get('convert/:celsius')
  convertByParam(@Param('celsius') celsius: string) {
    return this.temperatuerService.convert(parseFloat(celsius));
  }
}
